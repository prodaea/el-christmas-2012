/**
 * ...
 * @author RLC
 */
var Christmas = function() {
    this.hasFlash = false;
    this.dataQueue = [];

    this.buildFlash();
    this.openSocket();
};

Christmas.prototype.buildFlash = function buildFlash() {
    var now = new Date();
    
    var flashvars = {
    };
    var params = {
        menu: "false",
        scale: "noScale",
        allowFullscreen: "true",
        allowScriptAccess: "always",
        bgcolor: "",
        wmode: "direct" // can cause issues with FP settings & webcam
    };
    var attributes = {
        id:"elChristmas2012",
        name:"elChristmas2012"
    };
    swfobject.embedSWF(
        "swf/el-christmas-2012.swf?t=" + now.getTime(),
        "altContent", "100%", "100%", "10.0.0",
        "expressInstall.swf",
        flashvars, params, attributes);
};

Christmas.prototype.openSocket = function openSocket() {
    this.socket = io.connect();
    this.socket.on('areYouBored', function amIBored() {
        if (this.isBored) {
            this.imBored();
        }
    });
    this.socket.on('addImage', this.onSocketAddImage.bind(this));
};

Christmas.prototype.onSocketAddImage = function onSocketAddImage(data) {
    this.isBored = false;
    if (this.hasFlash) {
        var swf = document.getElementById('elChristmas2012');
        if (this.dataQueue.length > 0) {
            for (var i = 0; i < this.dataQueue.length; i++) {
                swf.addImageData(this.dataQueue.shift());
            }
        }
        
        swf.addImageData(data);
    } else {
        this.dataQueue.push(data);
    }
};

Christmas.prototype.flashReady = function flashReady() {
    this.hasFlash = true;
};

Christmas.prototype.imBored = function imBored() {
    this.isBored = true;
    this.socket.emit('imBored');
};

window.Christmas = new Christmas();
