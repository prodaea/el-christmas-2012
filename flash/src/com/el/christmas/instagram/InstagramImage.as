package com.el.christmas.instagram
{
	import com.el.christmas.res.Polaroid;
    import flash.display.Bitmap;
    import flash.display.Loader;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.EventDispatcher;
	import flash.globalization.DateTimeFormatter;
    import flash.net.URLRequest;
	
	/**
     * ...
     * @author RLC
     */
    public class InstagramImage extends Polaroid
    {
		private var data:Object;
		
        private var _url:String;

        private var imageHeight:Number;
        private var imageWidth:Number;
		
		private var username:String;
		private var formatter:DateTimeFormatter;
		private var time:Date;

        public function InstagramImage(data:Object):void
        {
			this.data = data;
            this._url = this.data.images.low_resolution.url;
            this.imageHeight = this.data.images.low_resolution.height;
            this.imageWidth = this.data.images.low_resolution.width;
			this.username = "@" + this.data.user.username;
			this.time = new Date();
			// http://stackoverflow.com/questions/544186/flex-date-constructor-is-mis-converting-unix-time-stamps-argh
			this.time.setTime(Number(this.data.created_time) * 1000);
			
			this.formatter = new DateTimeFormatter("en-US");
			this.formatter.setDateTimePattern("MMMM D YYYY at L:NN A");
			
			this.usernameField.text = this.username;
			this.dateField.text = formatter.format(this.time);

            this.load();
        }

        private function load():void
        {
            var request:URLRequest = new URLRequest(this._url);
            var loader:Loader = new Loader();
            loader.load(request);
            loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete);
        }

        private function onLoaderComplete(evt:Event):void
        {
            var bmp:Bitmap = evt.target.content;
			this.photo.addChild(bmp);
			
            this.dispatchEvent(new InstagramEvent(InstagramEvent.NEW_IMAGE, this));
        }

        public function get url():String
        {
            return _url;
        }

    }

}
