package com.el.christmas.instagram
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author RLC
	 */
	public class InstagramEvent extends Event
	{
		static public const NEW_IMAGE:String = "newImage";
		static public const IMAGE_COMPLETE:String = "imageComplete";
		
		private var _image:InstagramImage;
		
		public function InstagramEvent(type:String, image:InstagramImage)
		{
			this._image = image;
			super(type);
		}
		
		public function get image():InstagramImage
		{
			return _image;
		}
	
	}

}

