package com.el.christmas.instagram
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.external.ExternalInterface;
	/**
     * InstagramClient listens to a set of JS/EI calls that enable real-time instagramming via a node server hosting the parent page.
     * @author RLC
     */
    public class InstagramClient extends EventDispatcher
    {
        private var images:Vector.<String>;

        public function InstagramClient():void
        {
            images = new Vector.<String>();
            if (ExternalInterface.available) {
                ExternalInterface.addCallback("addImageData", this.addImageData);
				ExternalInterface.call("Christmas.flashReady");
            }
        }

        public function addImageData(data:Object):void {
            var url:String = data.images.low_resolution.url;

            if (!this.vectorHasString(this.images, url)) {
                images.push(url);
                var image:InstagramImage = new InstagramImage(data);
                image.addEventListener(InstagramEvent.NEW_IMAGE, onNewImage);
            }
        }

        private function vectorHasString(vector:Vector.<String>, compare:String):Boolean
        {
            for each (var string:String in vector) {
                if (string == compare)
                    return true;
            }

            return false;
        }

        private function onNewImage(evt:InstagramEvent):void
        {
            this.dispatchEvent(new InstagramEvent(InstagramEvent.IMAGE_COMPLETE, evt.image));
        }
    }
}
