package com.el.christmas.tableau
{
	import com.el.christmas.instagram.InstagramImage;
	import com.el.christmas.res.*;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.events.KeyboardEvent;
    import flash.external.ExternalInterface;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author RLC
	 */
	public class Tableau extends Sprite
	{
		private static const MAX_SNOW:Number = 300;
		static public const MAX_IMAGES:Number = 15;
		
		private var snow:Vector.<SnowFlake>;
		private var screen:Rectangle;
		private var imageQueue:Vector.<InstagramImage>;
		private var images:Vector.<InstagramImage>;
		private var imagesLayer:Sprite;
		private var backgroundId:Number;
		private var backgrounds:Vector.<Bitmap>;
		private var background:Bitmap;
        private var isBored:Boolean;
		
		public function Tableau()
		{
            isBored = false;
			backgroundId = 0;
			backgrounds = new <Bitmap>[new Bitmap(new Background1()), new Bitmap(new Background2()), new Bitmap(new Background3()), new Bitmap(new Background4()), new Bitmap(new Background5())];
			
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(evt:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			//Initialize graphics
			background = backgrounds[backgroundId];
			this.addChild(background);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			snow = new Vector.<SnowFlake>();
			screen = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			imageQueue = new Vector.<InstagramImage>();
			images = new Vector.<InstagramImage>();
			imagesLayer = new Sprite();
			this.addChild(imagesLayer);
			
			this.addEventListener(FullScreenEvent.FULL_SCREEN, onFullScreen);
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onKeyUp(evt:KeyboardEvent):void
		{
			var key:Number = evt.keyCode;
			// left 37
			if (key == 37)
			{
				backgroundId--;
				if (backgroundId < 0)
				{
					backgroundId = backgrounds.length - 1;
				}
			}
			// right 39
			if (key == 39)
			{
				backgroundId++;
				if (backgroundId == backgrounds.length)
				{
					backgroundId = 0;
				}
			}
			
			this.removeChild(background);
			background = backgrounds[backgroundId];
			this.addChildAt(background, 0);
		}
		
		private function onFullScreen(evt:FullScreenEvent):void
		{
			screen = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
		}
		
		private function onEnterFrame(evt:Event):void
		{
			if (snow.length < Tableau.MAX_SNOW)
			{
				var i:Number = snow.push(new SnowFlake(screen)) - 1;
				this.addChild(snow[i]);
			}
			
			var wind:Number = (screen.width / 2) - this.mouseX;
			wind /= 160;
			
			for each (var flake:SnowFlake in snow)
			{
				flake.update(wind);
			}
			
			var newImage:InstagramImage;
			for (var n:Number = 0; n < images.length; n++)
			{
				images[n].y += 4;
				if (imageQueue.length > 0)
				{
					if (images[n].y > screen.bottom)
					{
						//imagesLayer.removeChild(images.splice(n, 1)[0]);
                        images.splice(n, 1);
						newImage = imageQueue.shift();
						images.push(newImage);
						imagesLayer.addChild(newImage);
					}
					else if (images.length < MAX_IMAGES)
					{
						newImage = imageQueue.shift();
						images.push(newImage);
						imagesLayer.addChild(newImage);
					}
				}
				else if (images[n].y > screen.bottom)
				{
					images[n].y = screen.top - images[n].height;
                    images.push(images.splice(n, 1)[0]);
				}
			}

            if (images.length > MAX_IMAGES / 2 && imageQueue.length == 0 && ExternalInterface.available && isBored) {
                isBored = false;
                ExternalInterface.call("Christmas.imBored");
            }

			moveImagesToUniquePlaces();
		}
		
		private function moveImagesToUniquePlaces():void
		{
			for each (var image1:InstagramImage in images) {
				for each (var image2:InstagramImage in images) {
					if (image1 != image2) {
						var image1Rect:Rectangle = image1.getRect(imagesLayer);
						var image2Rect:Rectangle = image2.getRect(imagesLayer);
						var intersection:Rectangle = image1Rect.intersection(image2Rect);
						
						if (intersection.height != 0 && intersection.width != 0) {
							if (image1.y + image1.height < 0) {
								image1.y -= image1.height;
                                randomizeX(image1, screen);
							} else if (image2.y - image2.height < 0) {
								image2.y -= image2.height;
                                randomizeX(image2, screen);
							}

                            moveImagesToUniquePlaces();
						}
					}
				}
			}
		}
		
		public function addImage(instagramImage:InstagramImage):void
		{
			randomizeX(instagramImage, screen);
			instagramImage.y = screen.top - instagramImage.height;
			
			if (images.length < 1)
			{
				imagesLayer.addChild(instagramImage);
				images.push(instagramImage);
			}
			else
			{
				imageQueue.push(instagramImage);
			}

            isBored = true;
		}
		
		private function randomizeX(displayObject:DisplayObject, area:Rectangle):void
		{
			var xMin:Number = area.left;
			var xMax:Number = area.right - displayObject.width;
			displayObject.x = Math.random() * (xMax - xMin + 1) + xMin;
		}
		
		private function onFadeOutComplete():void
		{
			this.removeChild(imageQueue.shift());
		}
	
	}

}
