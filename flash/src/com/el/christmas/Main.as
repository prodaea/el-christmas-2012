package com.el.christmas
{
    import com.el.christmas.instagram.InstagramClient;
    import com.el.christmas.instagram.InstagramEvent;
    import com.el.christmas.tableau.Tableau;
    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.display.StageDisplayState;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;

    /**
     * ...
     * @author RLC
     */
    public class Main extends Sprite
    {
        private var tableau:Tableau;

        public function Main():void
        {
            stage.addEventListener(MouseEvent.CLICK, onClick);

            if (stage)
                init();
            else
                addEventListener(Event.ADDED_TO_STAGE, init);
        }

        private function onClick(evt:MouseEvent):void
        {
            if (stage.displayState == StageDisplayState.NORMAL) {
                stage.displayState = StageDisplayState.FULL_SCREEN;
            } else {
                stage.displayState = StageDisplayState.NORMAL;
            }
        }

        private function init(evt:Event = null):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, init);

            stage.scaleMode = StageScaleMode.NO_BORDER;

            //Start Instagram Real-Time
            var instagram:InstagramClient = new InstagramClient();
            instagram.addEventListener(InstagramEvent.IMAGE_COMPLETE, onImageComplete);

            //Start View
            this.tableau = new Tableau();
            this.addChild(tableau);
        }

        private function onImageComplete(evt:InstagramEvent):void
        {
            this.tableau.addImage(evt.image);
        }

    }

}
