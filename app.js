/**
 * ...
 * @author RLC
 */
//INCLUDES
var express = require('express');
var util = require('util');
var fs = require('fs');
var url = require('url');
var Request = require('request');
var socket = require('socket.io');

//APP
var app = new express();
var port = (process.env.PORT || 3000);
var server = require('http').createServer(app);
var io = socket.listen(server);

//SETTINGS
// can change cloud9 urls, so use that to test which environment we're in.
var envUrl = (process.env.EL_URL || 'http://el-christmas-2012.prodaea.c9.io');
var imageWhitelist = {
    cloud9: "http://el-christmas-2012.prodaea.c9.io/",
    cloud9Dev: "http://el-christmas-2012.prodaea.c9.io/swf/el-christmas-2012.swf",
    heroku: "http://el-christmas-2012.herokuapp.com/",
    herokuDev: "http://el-christmas-2012.herokuapp.com/swf/el-christmas-2012.swf"
};

//INSTAGRAM SETTINGS
var Instagram = {};
//urls
Instagram.SUBSCRIPTIONS = 'https://api.instagram.com/v1/subscriptions/';

//secret stuff, shhh...
Instagram.CLIENT_ID = 'e384d8c649254d6c8c7c2df9e0cc1d84';
Instagram.CLIENT_SECRET = '10b37f07cfcf41419f434034a28320e2';

//settings
Instagram.FILTER_TAG = 'empathylab';
Instagram.RECENT_FILTER_TAGS = 'https://api.instagram.com/v1/tags/' + Instagram.FILTER_TAG + '/media/recent?count=15';
Instagram.NEXT_URL = '';

//server session
server.listen(port, function appListen() {
    console.log('Listening on port %d', port);
});

//socket io session
io.set('log level', 1);
io.on('connection', function onIoConnection(socket) {
    requestRecent();
//    socket.emit('areYouBored');
//    socket.on('isBored', function onIsBored() {
//        console.log('== is bored ==');
//        if (Instagram.NEXT_URL !== '') {
//            requestNext();
//        }
//    });
});

//METHODS
var onSubscriptionPostComplete = function onSubscriptionPostComplete(error, response, body) {
    var data = JSON.parse(body);
    
    //TODO: retry subscription on fail.
    console.log('onSubscriptionPostComplete -> data: ', data);
};

// Register handshake w/ Instagram
var startSubscription = function startSubscription(request) {
    Request.post(Instagram.SUBSCRIPTIONS, onSubscriptionPostComplete).form({
        client_id: Instagram.CLIENT_ID,
        client_secret: Instagram.CLIENT_SECRET,
        object: 'tag',
        aspect: 'media',
        object_id: Instagram.FILTER_TAG,
        callback_url: envUrl + '/callback'
    });
};

var onRequestRecent = function onReqeustRecent(error, response, body) {
    var data = JSON.parse(body).data;
    console.log('== image data ==');
    // Instagram.NEXT_URL = JSON.parse(body).pagination.next_url;
    for (var i = 0; i < data.length; i++) {
        var standard = data[i].images.standard_resolution;
        standard.url = envUrl + '/image?i=' + standard.url;
        
        var low = data[i].images.low_resolution;
        low.url = envUrl + '/image?i=' + low.url;
        
        io.sockets.emit('addImage', data[i]);
    }
};

// TODO: make a requestUrl method for both of these
var requestNext = function requestNext() {
    console.log('== request next ==');
    Request.get(Instagram.NEXT_URL, {
        qs: {
            client_id: Instagram.CLIENT_ID
        }
    }, onRequestRecent);
    Instagram.NEXT_URL = '';
}

var requestRecent = function requestRecent() {
    console.log('== request recent ==');
    Request.get(Instagram.RECENT_FILTER_TAGS, {
        qs: {
            client_id: Instagram.CLIENT_ID
        } 
    }, onRequestRecent);
};

var isValidReferer = function isValidReferer(refererUrl) {
    var parts = url.parse(refererUrl);
    var testUrl = parts.protocol + "//" + parts.hostname + parts.pathname;
    console.log('== REFERED: ' + testUrl + ' ==');
    
    if (testUrl === imageWhitelist.cloud9 || testUrl === imageWhitelist.cloud9Dev || testUrl === imageWhitelist.heroku || testUrl === imageWhitelist.herokuDev) {
        return true;
    }
    
    return false;
};

// Configure
app.configure(function appConfigure() {
    app.use(express.bodyParser());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.get('/image', function getImage(request, response) {
    console.log('== image ==');
    var imageUrl = request.query.i;
    if (isValidReferer(request.headers.referer)) {
        Request.get(imageUrl).pipe(response);
    } else {
        console.log('== INVALID REFERER ==');
        response.writeHead(404);
        response.end();
    }
});

app.get('/', function getRoot(request, response) {
    //This will start a subscription per request.
    // I don't recommend this, but i want to be able to try <ipaddress>/callback dynamically.
    startSubscription(request);
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write(fs.readFileSync('./index.html', 'utf8'));
    response.end();
});

app.get('/subscriptions/:id', function getSubscriptions(request, response) {
    console.log('== subscriptions not yet implemented ==');
});

app.get('/callback', function getCallback(request, response) {
    // why doesn't !== work like it should here?
    if (request.param('hub.challenge') != null) {
        response.send(request.param('hub.challenge'));
    } else {
        console.log('ERROR on subscription request: %s', util.inspect(request));
    }
});

app.post('/callback', function postCallback(request, response) {
    var body = request.body;
    var notification = {};
    console.log('== tag callback ==');
    for (var i = 0; i < body.length; i++) {
        notification = body[i];
        requestRecent();
    }
    
    response.writeHead(200);
    response.end();
});
